﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinesController : MonoBehaviour
{
	private LineRenderer backLine;
	private LineRenderer frontLine;
	private Ray2D frontSlingshotRay;
	
	public void Init ()
	{
		backLine = App.Instance.view.slingshot.transform.GetChild(0).GetComponent<LineRenderer>();
		frontLine = App.Instance.view.slingshot.transform.GetChild(1).GetComponent<LineRenderer>();
		frontSlingshotRay = new Ray2D(frontLine.transform.position, Vector2.zero);
		RenderLines();
	}
	
	private void RenderLines()
	{
		backLine.SetPosition(0, backLine.transform.position);
		frontLine.SetPosition(0, frontLine.transform.position);
		backLine.sortingOrder = 0;		
		frontLine.sortingOrder = 2;
	}

	public void LinesUpdate(GameObject projectile)
	{
		var width = projectile.GetComponent<CircleCollider2D>().radius;
		Vector2 distance = projectile.transform.position - frontLine.transform.position;
		frontSlingshotRay.direction = distance;
		Vector2 point = frontSlingshotRay.GetPoint(distance.magnitude + width);
		backLine.SetPosition(1, point);
		frontLine.SetPosition(1, point);
	}

	public void EnableDisableLines(bool enable)
	{
		frontLine.enabled = enable;
		backLine.enabled = enable;
	}
}
