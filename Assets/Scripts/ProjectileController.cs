﻿using System;
using System.Collections;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
	[SerializeField] private float maximumStretch;
	[SerializeField] private float maximumSpeed;
	private Transform slingshot;
	private GameObject projectile;
	private Rigidbody2D projectileRB;
	private SpringJoint2D springJoint;
	private Ray2D mouseRay;
	private Vector2 speed;
	
	public bool isClicked;
	public void Init ()
	{
		slingshot = App.Instance.view.slingshot.transform;
		SpawnProjectile();
	}

	private void Update()
	{
		if(isClicked)
			FollowMouse();
		
		if (springJoint != null)
		{
			if (!projectileRB.isKinematic && speed.sqrMagnitude > projectileRB.velocity.sqrMagnitude)
			{
				Destroy(springJoint);
				if (speed.magnitude > maximumSpeed)
					projectileRB.velocity = speed.normalized*maximumSpeed;
				else 
				projectileRB.velocity = speed;
			}

			if (!isClicked)
			{
				speed = projectileRB.velocity;
			}
			App.Instance.controller.lines.LinesUpdate(projectile);
		}
		else
		{
			App.Instance.controller.lines.EnableDisableLines(false);
			StartCoroutine(DestroyProjectile());
		}

	}

	private void SpawnProjectile()
	{
		isClicked = false;
		projectile = Instantiate(App.Instance.model.projectilesModel.ProjectilePrefabs[0]);
		projectileRB = projectile.GetComponent<Rigidbody2D>();
		springJoint = projectile.GetComponent<SpringJoint2D>();
		projectileRB.isKinematic = true;
		projectile.transform.SetParent(slingshot);
		projectile.transform.localPosition = new Vector2(-2, 0);
		springJoint.connectedBody = slingshot.GetComponent<Rigidbody2D>();
		mouseRay = new Ray2D(slingshot.position, Vector2.zero);

	}

	private void FollowMouse()
	{
		Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		
		Vector2 distance = mousePosition - new Vector2(slingshot.position.x, slingshot.position.y);

		if (distance.sqrMagnitude > Mathf.Pow(maximumStretch, 2))
		{
			mouseRay.direction = distance;
			mousePosition = mouseRay.GetPoint(maximumStretch);
		}
		
		projectile.transform.position = mousePosition;
	}

	private IEnumerator DestroyProjectile()
	{
		yield return new WaitForSeconds(1);
		Destroy(projectile);
		SpawnProjectile();		
		App.Instance.controller.lines.EnableDisableLines(true);
	}
}
