﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHandler : MonoBehaviour
{

    private SpringJoint2D springJoint;
    private Rigidbody2D projectileRB;
    
    void Start()
    {
        springJoint = GetComponent<SpringJoint2D>();
        projectileRB = GetComponent<Rigidbody2D>();
    }
    
    private void OnMouseDown()
    {
        springJoint.enabled = false;
        App.Instance.controller.projectile.isClicked = true;

    }

    private void OnMouseUp()
    {
        springJoint.enabled = true;
        projectileRB.isKinematic = false; 
        App.Instance.controller.projectile.isClicked = false;
    }
}
