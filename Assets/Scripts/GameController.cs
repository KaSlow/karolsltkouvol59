﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	void Start () {
		App app = App.Instance;
		App.Instance.controller.projectile.Init();
		App.Instance.controller.lines.Init();
	}
}
