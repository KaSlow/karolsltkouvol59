﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Projectiles", menuName = "Custom/Projectiles", order = 0)]
public class Projectiles : ScriptableObject
{
	public GameObject[] ProjectilePrefabs;
}
