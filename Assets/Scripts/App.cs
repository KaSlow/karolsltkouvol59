﻿using System;
using UnityEngine;
using System.Collections;

public class App : MonoBehaviour
{
    public static App Instance;

    [Serializable]
    public class View
    {
        public GameObject slingshot;
    }

    [Serializable]
    public class Model
    {
        public Projectiles projectilesModel;
    }

    [Serializable]
    public class Controller
    {
        public ProjectileController projectile;
        public LinesController lines;
    }

    public Model model;
    public View view;
    public Controller controller;


    void Awake()
    {
        App.Instance = this;
    }

}